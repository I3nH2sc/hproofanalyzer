# basics for resolution

include("resobase.jl")

# make_binding2
function make_binding2(vl1, vl2)
  if length(vl1) != length(vl2); return end
  b = Subst()
  map((v1,v2)->b[v1]=v2,vl1,vl2)
  return b
end

# get_binding
function get_binding(σ::Subst)
  for s in keys(σ)
    if s != σ[s]; delete!(σ, s) end
  end
  return σ
end

# represent_*
function represent_term(b::Subst, tm::Term)
  return mapexpr(x->get(b,x,x), tm) # constant are itself
end

function represent_as(b, exp)
  map(tm->represent_term(b, tm), exp)
end

# equal on Subst
""" 
equalSubst may no need. because any Dict seems sorted on keys
"""
function equalSubst(σ1::Subst, σ2::Subst)
#  if length(keys(σ1)) != length(keys(σ2)); return false end
#  if collect(keys(σ1)) != collect(keys(σ2)); return false end
  if sort(collect(keys(σ1))) != sort(collect(keys(σ2))); return false end

  for k1 in keys(σ1)
    if σ1[k1] != σ2[k1]; return false end
  end
  return true
end


