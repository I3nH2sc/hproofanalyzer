# test for test_resobase.jl
using Test

include("resobase.jl")

println("\n... test resobase.jl")

@testset "is" begin
  @test isconst(Subst(), :a)
  @test isconst(Subst(:x=>:x, :y=>:y), :a)
  @test isconst(Subst(:x=>:x, :y=>:y), 123)
  @test !isvar(Subst(), :x)
  @test isvar(Subst(:x=>:x, :y=>:y), :x)
  @test isvar(Subst(:x=>:x, :y=>:y), :y)
  @test !isvar(Subst(:x=>:x, :y=>:y), :(f(x,y)))
  @test !isconst(Subst(:x=>:x, :y=>:y), :(f(x,y)))
end

@testset "make_binding" begin
# b0 = make_binding([])
# @test false == isvar(b0, :x)  

 b1 = Subst(:x=>:x) #make_binding([:x])
 @test b1[:x] == :x
 @test true  == isvar(b1, :x)
 @test false == isvar(b1, :y)

 b2 = make_binding([:x,:y,:z])
 @test b2[:x] == :x
 @test b2[:y] == :y
 @test b2[:z] == :z

 @test true  == isvar(b2, :x)
 @test true  == isvar(b2, :y)
 @test true  == isvar(b2, :z)
 @test false == isvar(b2, :a)

 @test false == isvar(b2, :(f(x)))
end


@testset "mapexpr" begin
 fn0(x) = x
 @test mapexpr(fn0, Meta.parse("P(x,f(y))")) == Meta.parse("P(x,f(y))")

 b2 = Dict{Symbol, Term}(:x=>:a,:y=>:b)
 fn2(x) = get(b2,x,x)
 @test mapexpr(fn2, Meta.parse("P(x,f(y))")) == Meta.parse("P(a,f(b))")
 @test mapexpr(fn2, Meta.parse("P(x,f(y,x))")) == Meta.parse("P(a,f(b,a))")

end

@testset "sapply" begin
  @test sapply(Subst(),(:(P(x)))::Term) == :(P(x))
  @test sapply(Subst(:x=>:a),:x) == :a
  @test sapply(Subst(:x=>3),:x) == 3
  @test sapply(Subst(:x=>:a),:(x)) == :(a)
  @test sapply(Subst(:x=>:a),:(P(x))) == :(P(a))
  @test sapply(Subst(:x=>5),:(P(x))) == :(P(5))
  @test sapply(Subst(:x=>:a, :y=>:b),:(P(x,f(y)))) == :(P(a,f(b)))

  @test sapply(Subst(:x=>:(f(a,b)), :y=>:b),:(P(x,g(y,b)))) == :(P(f(a,b),g(b,b)))
  @test sapply(Subst(:x=>:(f(a,b)), :y=>:b, :z=>:w),:(P(x,g(y,w),z))) == :(P(f(a,b),g(b,w),w))

  @test sapply(Subst(:x=>:(f(a,b)), :y=>:b, :z=>:w),[:(P(x,g(y,w),z)),:(f(y,y))]) == [:(P(f(a,b),g(b,w),w)),:(f(b,b))]

  @test sapply(Subst(:x=>:(f(a,b)), :y=>:b, :z=>:w),[:(P(x,g(y,w),z)),:x,:(f(y,y))]) == [:(P(f(a,b),g(b,w),w)),:(f(a,b)),:(f(b,b))]

  @test sapply(Subst(:x=>:(f(4,b)), :y=>:b, :z=>7),[:(P(x,g(y,w),z)),:x,:(f(y,y))]) == [:(P(f(4,b),g(b,w),7)),:(f(4,b)),:(f(b,b))]

# [:x,:y] <- [:a,:x] is incorrect. 
  @test sapply(Subst(:x=>:a,:y=>:x),:(P(x,y))) == :(P(a,x))
  @test sapply(Subst(:x=>:a,:y=>:x),:(P(x,y))) != :(P(a,a))
end


@testset "sapply on σxσ" begin
  @test sapply(Subst(:x=>:x, :y=>:y), Subst(:x=>:x, :y=>:y)) == Subst(:x=>:x, :y=>:y)
  @test sapply(Subst(:x=>:x, :y=>:y), Subst(:x=>:b, :y=>:y)) == Subst(:x=>:b, :y=>:y)
  @test sapply(Subst(:x=>:a, :y=>:y), Subst(:x=>:x, :y=>:y)) == Subst(:x=>:a, :y=>:y)
  @test sapply(Subst(:x=>:a, :y=>:y), Subst(:x=>:x, :y=>:b)) == Subst(:x=>:a, :y=>:b)

  @test sapply(Subst(:x=>:a, :y=>:y), Subst(:x=>:x, :y=>:b)) == Subst(:x=>:a, :y=>:b)

  @test sapply(Subst(:x=>:(f(y)),:y=>:b), Subst(:x=>:(g(z)),:z=>:c)) == Subst(:x=>:(f(y)), :y=>:b)

  @test sapply(Subst(:x=>:(f(y)),:y=>:y), Subst(:x=>:x,:y=>:a)) == Subst(:x=>:(f(a)), :y=>:a)


end

@testset "sapply2" begin
  @test sapply2(Subst(:x=>:a), Subst(:y=>:b)) == Subst(:x=>:a, :y=>:b)
  @test sapply2(Subst(:x=>:a,:y=>:b), Subst(:x=>:x,:z=>:c)) == Subst(:x=>:a, :y=>:b,:z=>:c)
  @test sapply2(Subst(:x=>:a,:y=>:b), Subst(:x=>:a,:z=>:c)) == Subst(:x=>:a, :y=>:b, :z=>:c)
  @test sapply2(Subst(:x=>:a,:y=>:b), Subst(:x=>:a,:z=>:c)) == Subst(:x=>:a, :y=>:b, :z=>:c)

  @test sapply2(Subst(:x=>:(f(y)),:y=>:b), Subst(:x=>:(g(z)),:z=>:c)) == Subst(:x=>:(f(y)), :y=>:b,:z=>:c)

  @test sapply2(Subst(:x=>:(f(y)),:y=>:b), Subst(:x=>:(f(b)),:y=>:b)) == Subst(:x=>:(f(b)), :y=>:b)

  @test sapply2(Subst(:x=>:(f(y)),:y=>:b), Subst(:x=>:x,:y=>:y)) != Subst(:x=>:(f(b)), :y=>:b)

  @test sapply2(Subst(:x=>:x,:y=>:y), Subst(:x=>:(f(y)),:y=>:(f(x)))) == Subst(:x=>:(f(y)), :y=>:(f(x)))

end

@testset "fp_subst" begin
  @test fp_subst(Subst(:x=>:x, :y=>:y)) == Subst(:x=>:x,:y=>:y)
  @test fp_subst(Subst(:x=>:x, :y=>:a)) == Subst(:x=>:x,:y=>:a)
  @test fp_subst(Subst(:x=>:(f(y)), :y=>:a)) == Subst(:x=>:(f(a)),:y=>:a)
  @test fp_subst(Subst(:x=>:a, :y=>:(f(x)))) == Subst(:x=>:a,:y=>:(f(a)))
  @test fp_subst(Subst(:x=>:a,:y=>:(f(x,z)),:z=>:b)) == Subst(:x=>:a,:y=>:(f(a,b)),:z=>:b)

  @test fp_subst(sapply2(Subst(:x=>:(f(y)),:y=>:b), Subst(:x=>:(g(z)),:z=>:c))) == Subst(:x=>:(f(b)), :y=>:b,:z=>:c)

  @test fp_subst(Subst(:x=>:x,:y=>:(f(g(z,w),h(w))),:z=>:(k(w)),:w=>:(m(a)))) == Subst(:x=>:x,:y=>:(f(g(k(m(a)),m(a)),h(m(a)))),:z=>:(k(m(a))),:w=>:(m(a)))

  @test fp_subst(Subst(:x=>:(f(y)),:y=>:(g(z)),:z=>:(h(u)),:u=>:(k(a)))) == Subst(:x=>:(f(g(h(k(a))))),:y=>:(g(h(k(a)))),:z=>:(h(k(a))),:u=>:(k(a)))

  @test fp_subst(sapply2(Subst(:x=>:(f(y)),:y=>:b), Subst(:x=>:x,:y=>:y))) == Subst(:x=>:(f(b)), :y=>:b)

## next never occur
## substitution was made by unification,
## and it should be type A. so this is never case.
#  @test_throws Loop fp_subst(Subst(:x=>:(f(y)),:y=>:(f(x))))
   
end

@testset "loopcheck" begin
  @test false == loopcheck(:x, :y)

  @test_throws Loop loopcheck(:x, :x)
  @test_throws Loop loopcheck(:x, :(f(x)))

  @test_throws Loop loopcheck(:x, :(f(x,h(x))))
  @test_throws Loop loopcheck(:x, :(f(y,g(x))))


end

@testset "unification pure" begin
  @test_throws Fail(:a,:b,:unifySS)  unification(Subst(:x=>:x,:y=>:y),:a, :b)

  @test unification(Subst(:x=>:x, :y=>:y), :x, :x) == Subst(:x=>:x, :y=>:y)
  @test unification(Subst(:x=>:x, :y=>:y), :x, :y) == Subst(:x=>:y, :y=>:y)
  @test unification(Subst(:x=>:x, :y=>:y), :a, :y) == Subst(:x=>:x, :y=>:a)
  @test unification(Subst(:x=>:x, :y=>:y), :x, :a) == Subst(:x=>:a, :y=>:y)

  @test unification(Subst(:x=>:x, :y=>:y), :x, :(f(y,b))) == Subst(:x=>:(f(y,b)),:y=>:y)
  @test unification(Subst(:x=>:x, :y=>:y), :(f(x,b)), :y) == Subst(:x=>:x, :y=>:(f(x,b)))
  @test unification(Subst(:x=>:x, :y=>:y), :(f(x,b)), :(f(x,b))) == Subst(:x=>:x, :y=>:y)

  @test_throws Loop unification(Subst(:x=>:x, :y=>:y), :x, :(f(x,b)))
  @test_throws Loop unification(Subst(:x=>:x, :y=>:y), :(f(x,b)), :x)
  @test_throws Loop unification(Subst(:x=>:x, :y=>:y), :(f(g(x,c),b)), :(f(x,b)))
  @test_throws Fail unification(Subst(:x=>:x, :y=>:y), :(f(x,y)), :(g(x,y)))
  @test_throws Fail unification(Subst(:x=>:x, :y=>:y), :(f(x,y)), :(f(x,y,y)))

  @test unification(Subst(:x=>:x, :y=>:y), :(f(x,y)), :(f(x,y))) == Subst(:x=>:x, :y=>:y)
  @test unification(Subst(:x=>:x, :y=>:y), :(f(a,y)), :(f(x,b))) == Subst(:x=>:a, :y=>:b)
  @test unification(Subst(:x=>:x, :y=>:y), :(f(g(a),y)), :(f(x,b))) == Subst(:x=>:(g(a)), :y=>:b)
  @test unification(Subst(:x=>:x, :y=>:y), :(f(g(y),y)), :(f(x,b))) == Subst(:x=>:(g(b)), :y=>:b)

  @test unification(Subst(:x=>:x,:y=>:y,:z=>:z), :(f(g(y,z),h(z,y))), :(f(x,h(a,b)))) == Subst(:x=>:(g(b,a)),:y=>:b,:z=>:a)

  @test unification(Subst(:x=>:x,:y=>:y,:z=>:z,:w=>:w), :(f(g(y,z),h(z,y))), :(f(x,h(h(w),b))) ) == Subst(:x=>:(g(b,h(w))),:y=>:b,:z=>:(h(w)),:w=>:w)
end

@testset "unification intermezzo" begin
  @test unify(Subst(:x=>:x,:y=>:y,:z=>:z), :(P(x,f(x,y))), :(P(a,z))) == Subst(:y=>:y, :x=>:a, :z=>:(f(a,y)))
  @test unification(Subst(:x=>:x,:y=>:y,:z=>:z), :(P(x,f(x,y))), :(P(a,z))) == Subst(:y=>:y, :x=>:a, :z=>:(f(a,y)))

  @test unification(Subst(:x=>:x,:y=>:y,:z=>:z,:w=>:a), :(f(g(y,z),h(z,y))), :(f(x,h(h(a),b))) )== Subst(:x=>:(g(b,h(a))),:y=>:b,:z=>:(h(a)),:w=>:a)

  @test unification(Subst(:x=>:x,:y=>:y,:z=>:z,:w=>:a), :(f(g(y,z),h(z,y))), :(f(x,h(h(w),b))) )== Subst(:x=>:(g(b,h(a))),:y=>:b,:z=>:(h(a)),:w=>:a)

  @test unification(Subst(:x=>:x,:y=>:y,:z=>:z,:u=>:u),:(P(x,y,z,u)),:(P(f(y),g(z),h(u),k(a)))) == Subst(:x=>:(f(g(h(k(a))))),:y=>:(g(h(k(a)))),:z=>:(h(k(a))),:u=>:(k(a)))
 @test unification(Subst(:x=>:x,:y=>:y,:z=>:z), :(P(x,x,a)), :(P(y,z,z)))== Subst(:x=>:a, :y=>:a, :z=>:a)

 @test unification(Subst(:x=>:x,:y=>:y,:z=>:z), :(P(x,x,f(a))), :(P(y,z,f(z))))== Subst(:x=>:a, :y=>:a, :z=>:a)

 @test unification(Subst(:x=>:x,:y=>:y,:z=>:z,:u=>:u), :(P(x,x,u,f(u))), :(P(y,z,a,f(z)))) ==  Subst(:x=>:a, :y=>:a, :z=>:a, :u=>:a)

 @test unification(Subst(:x=>:x,:y=>:y,:z=>:z,:w=>:w,:u=>:u,:v=>:v), :(P(y,y,a,x,f(y))), :(P(z,u,u,f(z),x))) == Subst(:x=>:(f(a)), :y=>:a, :z=>:a, :w=>:w, :u=>:a,:v=>:v)

 @test unification(Subst(:x=>:x,:y=>:y,:z=>:z,:w=>:w,:u=>:u,:v=>:v), :(P(f(y),y,y,a)), :(P(f(z),z,u,u))) ==  Subst(:x=>:x, :y=>:a, :z=>:a, :w=>:w, :u=>:a,:v=>:v)

 @test unification(Subst(:x=>:x,:y=>:y,:z=>:z,:w=>:w,:u=>:u,:v=>:v), :(P(f(y),y,y,a,v,v)), :(P(f(z),w,w,u,u,w))) == Subst(:x=>:x, :y=>:a, :z=>:a, :w=>:a, :u=>:a, :v=>:a)
end


@testset "unification subst" begin
 @test unification(Subst(), Subst(),Subst()) == Subst()
 @test unification(Subst(), Subst(:x=>:x),Subst(:y=>:y)) == Subst(:x=>:x,:y=>:y)

 @test unification(Subst(), Subst(:x=>:a, :z=>:z),Subst(:x=>:a, :y=>:y)) == Subst(:x=>:a,:y=>:y, :z=>:z) 

 @test_throws Fail unification(Subst(), Subst(:x=>:a),Subst(:x=>:b))
 @test_throws Fail unification(Subst(), Subst(:x=>:x,:y=>:(g(x))),Subst(:x=>:x,:y=>:(f(x)))) 

 @test unification(Subst(), Subst(:x=>:a),Subst(:x=>:x,:y=>:x)) == Subst(:x=>:a,:y=>:a)
 @test unification(Subst(), Subst(:x=>:a),Subst(:x=>:y,:y=>:y)) == Subst(:x=>:a,:y=>:a)
 @test unification(Subst(), Subst(:x=>:a),Subst(:x=>:y,:y=>:a)) == Subst(:x=>:a,:y=>:a)

 @test unification(Subst(), Subst(:x=>:x,:y=>:y),Subst(:x=>:x,:z=>:z)) == Subst(:x=>:x,:y=>:y,:z=>:z)
 @test unification(Subst(), Subst(:x=>:x,:y=>:(f(x))),Subst(:x=>:x,:z=>:(f(x)))) == Subst(:x=>:x,:y=>:(f(x)),:z=>:(f(x)))


 @test unification(Subst(), Subst(:x=>:(f(y)),:y=>:y),Subst(:x=>:(f(y)),:y=>:y)) == Subst(:x=>:(f(y)),:y=>:y)

 @test unification(Subst(), Subst(:x=>:(f(y)),:y=>:y),Subst(:x=>:(f(z)),:z=>:z)) == Subst(:x=>:(f(z)),:y=>:z,:z=>:z)


# not type A
 @test unification(Subst(), Subst(:x=>:(f(y)),:y=>:w, :w=>:a), Subst(:x=>:(f(z)),:z=>:u, :u=>:u, :v=>:v)) ==  Subst(:x=>:(f(a)), :y=>:a, :z=>:a, :w=>:a, :u=>:a, :v=>:v)

 @test unification(Subst(), Subst(:x=>:(f(y)),:y=>:(g(w,w)), :w=>:a), Subst(:x=>:(f(z)),:z=>:(g(u,v)), :u=>:u, :v=>:v)) == Subst(:x=>:(f(g(a,a))), :y=>:(g(a,a)), :z=>:(g(a,a)),:w=>:a, :u=>:a, :v=>:a)


end

#@testset "Δ get all disagreements" begin
 @test [] == Δ(Subst(), :a, :a)
 @test_throws Fail Δ(Subst(), :a, :b)
 @test [(:a,:x)]==Δ(Subst(:x=>:x), :a, :x)
 @test [(:a,:x),(:y,:b)]==Δ(Subst(:x=>:x, :y=>:y), :(f(a,y)), :(f(x,b)))
#end

@testset "δ decides direction" begin
#  @test_throws Fail δ(Subst(), :a, :b)
  @test Subst(:x=>:b) == δ(Subst(:x=>:x), :x, :b)
  @test Subst(:x=>:y,:y=>:y) == δ(Subst(:x=>:x,:y=>:y), :x, :y)
  @test Subst(:x=>:b,:y=>:b) == δ(Subst(:x=>:x,:y=>:b), :x, :y)
#  @test Subst(:x=>:(f(y)),:y=>:y) == δ(Subst(:x=>:x,:y=>:y), :x, :(f(y)))
  @test Subst(:x=>:a) == δ(Subst(:x=>:x), :a, :x)
#  @test Subst(:x=>:(f(y)),:y=>:y) == δ(Subst(:x=>:x,:y=>:y), :(f(y)), :x)
#  @test Subst(:x=>:x,:y=>:(g(x))) == δ(Subst(:x=>:x,:y=>:y), :(f(y)), :(f(g(x))))
end

