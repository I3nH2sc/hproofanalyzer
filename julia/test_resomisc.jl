# test for test_misc.jl

using Test

include("resomisc.jl")

println("\n... test resomisc.jl")

@testset "make_binding2" begin
  b1 = make_binding2([:x],[:a])
  @test b1[:x] == :a
  b2 = make_binding2([:x,:y,:z],[:a,:b,:c])
  @test b2[:x] == :a
  @test b2[:y] == :b
  @test b2[:z] == :c
end

@testset "get_binding" begin
# this make binding from substitution
# it may contain dont appeard in the exp while exp = sapply(sigma, e)

  @test get_binding(Subst(:x=>:x,:y=>:(f(x)))) == Subst(:x=>:x)
  @test get_binding(Subst(:x=>:x,:y=>:y)) == Subst(:x=>:x,:y=>:y)
  @test get_binding(Subst(:x=>:x,:z=>:a,:y=>:y)) == Subst(:x=>:x,:y=>:y)
end

@testset "represent_as" begin
 b0 = Subst()
 @test represent_as(b0, [:y,:x,:x,:z]) == [:y,:x,:x,:z]

 b1 = make_binding2([:x,:y,:z],[:a,:b,:c]) 
 @test represent_as(b1, [:y,:x,:x,:z]) == [:b,:a,:a,:c]
# @test represent_as(b1, :(P(x,y,x))) == :(P(a,b,a))
end

@testset "equalsubst" begin
 @test equalSubst(Subst(), Subst())
 @test equalSubst(Subst(:x=>:x), Subst(:x=>:x))
 @test equalSubst(Subst(:y=>:y,:x=>:x), Subst(:x=>:x,:y=>:y))
 @test !equalSubst(Subst(:y=>:y), Subst(:x=>:x))
 @test equalSubst(Subst(:x=>:a), Subst(:x=>:a))
 @test !equalSubst(Subst(:x=>:x), Subst(:x=>:x,:y=>:y))
 @test !equalSubst(Subst(:y=>:y,:x=>:x), Subst(:x=>:x))

 @test equalSubst(Subst(:x=>:(f(x))), Subst(:x=>:(f(x))))
 @test !equalSubst(Subst(:x=>:(f(x))), Subst(:x=>:(f(y))))

 @test equalSubst(Subst(:y=>:y,:x=>:(f(x))), Subst(:x=>:(f(x)),:y=>:y))
 @test equalSubst(Subst(:y=>:b,:z=>:c,:x=>:a), Subst(:z=>:c,:x=>:a, :y=>:b))

 @test equalSubst(Subst(:y=>:y,:x=>:(f(y))), Subst(:x=>:(f(y)),:y=>:y))

end

