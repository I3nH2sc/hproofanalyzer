# basics for resolution

include("resotype.jl")

### make_binding from vars to subst
function make_binding(vars)
  bb = Dict{Symbol, Term}()
  map(x->bb[x] = x, vars)
  return bb
end


### mapexpr applys fn over a Expr
function mapexpr(fn::Function, term::Symbol) 
  fn(term)
end

function mapexpr(fn::Function, term::Number) 
  fn(term)
end

function mapexpr(fn::Function, expr::Expr)
  expr.args = map(ag->mapexpr(fn, ag), expr.args)
  expr
end

function mapexpr(fn::Function, ta::Array)
  map(e->mapexpr(fn,e), ta)
end

### sapply substitute σ to expr
function sapply(σ::Subst, exp)
  fn(x) = get(σ, x, x)
  mapexpr(fn, exp)
end

### sapply σ on σ
function sapply(σ1::Subst, σ2::Subst)::Subst
  for k in keys(σ1)
    σ1[k] = sapply(σ2, σ1[k])
  end
  return σ1
end

function sapply2(σ1::Subst, σ2::Subst)::Subst
 σ12 = merge(σ2, σ1)
 sapply(σ12, σ2)
end

### basic symbol predicates
isSymbol(x) = isa(x, Symbol)

isvar(b::Subst, s::Any) =  s in keys(b)

isconst(σ::Subst, sym::Expr)::Bool = false
isconst(σ::Subst, sym::Number)::Bool = true
isconst(σ::Subst, sym::Symbol)::Bool = isSymbol(sym) && !isvar(σ, sym) 

### loopcheck
function loopcheck(var::Symbol, sym::Symbol)
  (var == sym) && throw(Loop(var, sym, loopcheck))
  return false
end

function loopcheck(var::Symbol, term::Expr)
# it is not loopcheck when var == term. but caller know it.
  map(tm->loopcheck(var,tm), term.args)
  return false
end

### unify two expression
function unify(σ::Subst, exp1::Symbol, exp2::Symbol)
  exp1 = sapply(σ, exp1)
  exp2 = sapply(σ, exp2)
  if exp1 != exp2 
    if isvar(σ,exp1) 
      σ[exp1] = exp2
    elseif isvar(σ,exp2) # exp1 is a const and exp2 is a var
      σ[exp2] = exp1
    else   # both const and not same
      throw(Fail(exp1, exp2, :unifySS))
    end     
  end
  return σ 
end

function unify(σ::Subst, exp1::Symbol, exp2::Expr)
  exp1 = sapply(σ, exp1)
  exp2 = sapply(σ, exp2)
  if isvar(σ, exp1)
    loopcheck(exp1, exp2) # check inside loop
    σ[exp1] = exp2
    return σ
  end
  throw(Fail(exp1, exp2, :unifySE))
end

function unify(σ::Subst, exp1::Expr, exp2::Symbol)
  unify(σ, exp2, exp1)
end

function unify(σ::Subst, exp1::Expr, exp2::Expr)
  # function form is regarded as an vector. A function symbol may be a var.
  length(exp1.args) != length(exp2.args) && throw(Fail(exp1, exp2, :unifyEE))
  exp1 = sapply(σ, exp1)
  exp2 = sapply(σ, exp2)
  nσ = σ
  for ix in 1:length(exp1.args)
    a1 = exp1.args[ix]
    a2 = exp2.args[ix]
    dσ=unify(nσ, sapply(nσ,a1),sapply(nσ,a2)) # sapply need??
    nσ = sapply(nσ, dσ)
  end
  return nσ
end

"""
unify of substs
"""
function unify(σ::Subst, σ1::Subst, σ2::Subst)
  v1 = keys(σ1)
  v2 = keys(σ2)
  nσ = merge(σ1, σ2)
  for v in intersect(v1,v2)
    nt = σ1[v]
    if nt != σ2[v]
      nσ = unify(nσ, σ1[v], σ2[v])
    end
    nσ[v] = sapply(nσ, nt)
  end
  return nσ
end

"""
fp_subst find a fixed point of a substitution
"""
function fp_subst(σ::Subst) ## finding fixed point of σ
  cnt=0
  σb = σ
  while true
    σa = sapply(σ, σb)
    if σa == σb; break end  # this is fixed
    σb = σa
    cnt += 1
  end
  cnt>0 && println("fp loop count=$cnt")
  return σb
end

### main unification
"""
unification over exp or subst
"""
function unification(σ::Subst, exp1, exp2)::Subst
  fp_subst(unify(σ,exp1,exp2))
end

###### belows are experimental code
### δ determines the direction of disagreement
function δ(σ::Subst, exp1::Symbol, exp2::Symbol)
  exp1 = sapply(σ, exp1)
  exp2 = sapply(σ, exp2)
  if exp1 != exp2 
    if isvar(σ,exp1) 
      σ[exp1] = exp2
    elseif isvar(σ,exp2) # exp1 is a const and exp2 is a var
      σ[exp2] = exp1
    else   # both const and not same
      throw(Fail(exp1, exp2, :unifySS))
    end     
  end
  return σ 
end

function δ(σ::Subst, exp1::Symbol, exp2::Expr)
 exp1 = sapply(σ, exp1)
 exp2 = sapply(σ, exp2)
 if isvar(σ, exp1); Subst(exp1 => exp2)
 else throw(Fail(exp1, exp2, :δSE))
 end
end

function δ(σ::Subst, exp1::Expr, exp2::Symbol)
 exp1 = sapply(σ, exp1)
 exp2 = sapply(σ, exp2)
 if isvar(σ, exp2); Subst(exp2 => exp1)
 else throw(Fail(exp2, exp1, :δES))
 end
end

### Δ get all disagreements
function Δ(σ::Subst, exp1::Symbol, exp2::Symbol)
 if exp1 == exp2; return [] end
 if isvar(σ,exp1) || isvar(σ,exp2); return[(exp1,exp2)] end
 throw(Fail(exp1,exp2,:Δ)) 
end

function Δ(σ::Subst, exp1::Symbol, exp2::Expr)
 return [(exp1, exp2)]
end

function Δ(σ::Subst, exp1::Expr, exp2::Symbol)
 return [(exp1, exp2)]
end

function Δ(σ::Subst, exp1::Expr, exp2::Expr)
  length(exp1.args) != length(exp2.args) && throw(Fail(exp1, exp2, :unifyEE))
  nΔ = [] 
  for ix in 1:length(exp1.args)
    a1 = exp1.args[ix]
    a2 = exp2.args[ix]
    d=Δ(σ, a1,a2) 
    append!(nΔ, d)
  end
  return nΔ
end

